﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text scoreText;
    public Text victoryText;

    private Rigidbody rb;
    private int score;

    void Start() {
        rb = GetComponent<Rigidbody>();
        SetScore(0);
    }

    void SetScore(int newScore) {
        score = newScore;
        scoreText.text = "Score: " + score.ToString() + " / 13";
        if (score >= 13) {
            victoryText.gameObject.SetActive(true);
        } else {
            victoryText.gameObject.SetActive(false);
        }
    }

    void Update() {
        if (Input.GetKey("escape")) {
            Application.Quit();
        }
    }

    void FixedUpdate() {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("apple")) {
            other.gameObject.SetActive(false);
        }
        SetScore(score + 1);
    }

}
